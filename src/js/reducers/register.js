import { REGISTER__REQUEST, 
         REGISTER__RESPONSE, 
         REGISTER__FAILURE } from '../constants/actions'
import { REGISTER_STATUS_LOADING,
         REGISTER_STATUS_SUCCESS,
         REGISTER_STATUS_ERROR } from '../constants'

const register = (state = {}, action) => {
    switch (action.type){
        case REGISTER__REQUEST:
            return Object.assign({}, state, {
                status: REGISTER_STATUS_LOADING,
                message: action.message
            })
        case REGISTER__RESPONSE:
            return Object.assign({}, state, {
                status: action.status,
                message: action.message
            })
        case REGISTER__FAILURE:
            return Object.assign({}, state, {
                status: REGISTER_STATUS_ERROR,
                message: action.message
            })
        default:
            return state
    }
}

export default register
