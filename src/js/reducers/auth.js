import { LOGIN__REQUEST, 
         LOGIN__RESPONSE, 
         LOGIN__FAILURE } from '../constants/actions'
import { LOGIN_STATUS_LOADING,
         LOGIN_STATUS_SUCCESS,
         LOGIN_STATUS_ERROR } from '../constants'

const auth = (state = {}, action) => {
    switch (action.type){
        case LOGIN__REQUEST:
            return Object.assign({}, state, {
                status: LOGIN_STATUS_LOADING,
                message: action.message
            })
        case LOGIN__RESPONSE:
            return Object.assign({}, state, {
                status: action.status,
                message: action.message
            })
        case LOGIN__FAILURE:
            return Object.assign({}, state, {
                status: LOGIN_STATUS_ERROR,
                message: action.message
            })
        default:
            return state
    }
}

export default auth
