import { combineReducers } from 'redux'
import auth from './auth'
import nav from './nav'
import users from './users'
import register from './register'
import vendors from './vendors'
import food from './food'

// Merge the reducers
const appReducer = combineReducers({
    register,
    auth,
    nav,
    users,
    vendors,
    food
})

export default appReducer