import { NAV_SECTION_CHANGE, 
    DOC_EDIT_ENTER, 
    DOC_EDIT_EXIT,
    CONTENT_PUSH__SUCCESS, 
    DOC_REMOVE_NEW,
    DOC_ADD_NEW,
    CONTENT_ADD__SUCCESS} from '../constants/actions'

const auth = (state = {}, action) => {
    switch (action.type) {
        case NAV_SECTION_CHANGE:
            return Object.assign({}, state, {
                section: action.section
            })
        case DOC_EDIT_ENTER:
            return Object.assign({}, state, {
                editId: action.id
            })
        case DOC_EDIT_EXIT:
            return Object.assign({}, state, {
                editId: null
            })
        case DOC_REMOVE_NEW:
            return Object.assign({}, state, {
                new: false
            })
        case DOC_ADD_NEW:
            return Object.assign({}, state, {
                new: true
            })
        case CONTENT_ADD__SUCCESS:
        case CONTENT_PUSH__SUCCESS:
            return Object.assign({}, state, {
                editId: null,
                new: false
            })
        default:
            return state
    }
}

export default auth
