import { CONTENT_PUSH__SUCCESS,
    ADMIN_FETCH__SUCCESS, 
    CONTENT_ADD__SUCCESS,
    CONTENT_DELETE__SUCCESS} from '../constants/actions'

import { NAV_SECTION_FOOD } from '../constants'

const food = (state = [], action) => {
switch (action.type) {
    case CONTENT_PUSH__SUCCESS:
        if (action.section == NAV_SECTION_FOOD) {
            return state.map( (item, index) => {
                if (item._id !== action.item._id) {
                    return item
                }

                return {
                    ...item,
                    ...action.item,
                    location: {
                        ...item.location,
                        ...action.item.location
                    }
                }
            })
        }
        else {
            return state
        }
    case CONTENT_ADD__SUCCESS:
        if (action.section == NAV_SECTION_FOOD) {
            return [...state, action.item]
        }
        else {
            return state
        }
    case CONTENT_DELETE__SUCCESS:
        if (action.section == NAV_SECTION_FOOD) {
            return state.filter( (obj) => {
                return obj._id !== action.id
            })
        }
        else {
            return state
        }
    case ADMIN_FETCH__SUCCESS:
        console.log("Fethced food:", action.food)
        return Object.assign([], action.food)
    default:
        return state
}
}

export default food

