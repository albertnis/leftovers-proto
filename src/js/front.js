// Module imports
import React from 'react'
import { hydrate } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import axios from 'axios'

// File imports
import Root from './components/Root.jsx'
import appReducer from './reducers'
import appEpicMiddleware from './epics'

// Grab state as injected by server
const initialState = window.__PRELOADED_STATE__

// Initialise store
/* TODO: Remove logger middleware for production */
const loggerMiddleware = createLogger()
let store = createStore(
    appReducer,
    initialState,
    applyMiddleware(appEpicMiddleware,
                    thunkMiddleware, 
                    loggerMiddleware)
)

// Hook app into server-rendered 'shell' and take over
// Using client-side <Root /> component as imported
hydrate(
    <Root store={store} />,
    document.getElementById('root')
)