# Frontend JavaScript

Here lies the frontend Javascript for the application. Rendered components have the `.jsx` extension. 

Entry point: `front.js`.

Folders:

- **Actions**: Action definitions for dispatch from containers and epics.
- **Components**: React components to be rendered. These are used for server-side rendering also, with the exception of `Root.jsx` which has a specific server-side version in *js-backend*.
- **Containers**: "Shell" components which link the redux store to a component in the *components* folder.
- **Constants**: A whole heap of constants imported for consistency throughout the frontend and backend. Redux message constants are contained in the `actions.js` file.
- **Epics**: Basically event listeners which do all the AJAX work are in here. Combined in `index.js`.
- **Reducers**: State change handling for each state tag. Combined in `index.js`.