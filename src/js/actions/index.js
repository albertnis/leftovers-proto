/* TODO: Consider splitting into smaller files */
/* TODO: More robust server communication (error handling etc.) */

// Import a heck of a lot of constants
import { REGISTER__REQUEST,
         REGISTER__RESPONSE, 
         REGISTER__FAILURE,
         LOGIN__REQUEST,
         LOGIN__RESPONSE,
         LOGIN__FAILURE,
         NAV_SECTION_CHANGE,
         ADMIN_FETCH__REQUEST,
         ADMIN_FETCH__SUCCESS,
         DOC_EDIT_ENTER,
         DOC_EDIT_EXIT,
         DOC_ADD_NEW,
         DOC_REMOVE_NEW,
         CONTENT_PUSH__REQUEST, 
         CONTENT_PUSH__SUCCESS,
         CONTENT_ADD__SUCCESS,
         CONTENT_ADD__REQUEST,
         CONTENT_DELETE__REQUEST,
         CONTENT_DELETE__SUCCESS} from '../constants/actions'
import { REGISTER_STATUS_ERROR,
         LOGIN_STATUS_ERROR, 
         LOGIN_STATUS_SUCCESS} from '../constants'

/* REGISTRATION */

export const requestRegistration = (regcode) => {
    return {
        type: REGISTER__REQUEST,
        regcode: regcode,
        message: "Registering...",

    }
}

export const receiveRegistration = (response) => {
    if ((typeof(response.status) === "string") && (typeof(response.message) === "string")) {
        return {
            type: REGISTER__RESPONSE,
            status: response.status,
            message: response.message
        }
    }
    else {
        return {
            type: REGISTER__FAILURE,
            status: REGISTER_STATUS_ERROR,
            message: "A server error occurred."
        }
    }
}

export const registrationAjaxError = (error) => {
    return {
        type: REGISTER__FAILURE,
        status: REGISTER_STATUS_ERROR,
        message: "Could not contact server."
    }
}

/* LOGIN */

export const requestLogin = (username, password) => {
    return {
        type: LOGIN__REQUEST,
        username,
        password
    }
}

export const receiveLogin = (response) => {
    console.log(response)

    if ((typeof(response.status) === "string") && (typeof(response.message) === "string")) {
        if (response.status == LOGIN_STATUS_SUCCESS) {
            window.setTimeout(() => { window.location = "/admin" }, 1000)
        }
        return {
            type: LOGIN__RESPONSE,
            status: response.status,
            message: response.message
        }
    }

    else if (typeof(response.message) === "string") {
        return {
            type: LOGIN__RESPONSE,
            status: LOGIN_STATUS_ERROR,
            message: response.message
        }
    }

    else {
        return {
            type: LOGIN__FAILURE,
            status: LOGIN_STATUS_ERROR,
            message: "A server error occurred."
        }
    }
}

export const loginAjaxError = (error) => {
    console.log(error)
    return {
        type: LOGIN__FAILURE,
        status: LOGIN_STATUS_ERROR,
        message: "Could not contact server."
    }
}

/* SECTION CHANGE */

export const changeSection = (section) => {
    return {
        type: NAV_SECTION_CHANGE,
        section
    }
}

/* ADMIN FETCHING */

export const requestAdminFetch = () => {
    return {
        type: ADMIN_FETCH__REQUEST
    }
}

export const receiveAdminFetch = (response) => {
    console.log('Fetch response', response)
    return {
        type: ADMIN_FETCH__SUCCESS,
        users: response.users,
        vendors: response.vendors,
        food: response.food
    }
}

/* EDITING ENTITIES */

export const docEditEnter = (id) => {
    return {
        type: DOC_EDIT_ENTER,
        id
    }
}

export const docEditExit = () => {
    return {
        type: DOC_EDIT_EXIT
    }
}

/* ADDING/REMOVING ENTITIES */

export const docAddNew = () => {
    return {
        type: DOC_ADD_NEW
    }
}

export const docRemoveNew = () => {
    return {
        type: DOC_REMOVE_NEW
    }
}

/* SAVING EDITS TO SERVER */

export const requestDocSave = (flatItem, section) => {
    return {
        type: CONTENT_PUSH__REQUEST,
        section,
        flatItem
    }
}

export const receiveDocSave = (response) => {
    return {
        type: CONTENT_PUSH__SUCCESS,
        section: response.section,
        item: response.item
    }
}

/* SAVING ADDS TO SERVER */

export const requestDocAdd = (flatItem, section) => {
    return {
        type: CONTENT_ADD__REQUEST,
        section,
        flatItem
    }
}

export const receiveDocAdd = (response) => {
    return {
        type: CONTENT_ADD__SUCCESS,
        section: response.section,
        item: response.item
    }
}

/* SAVING DELETES TO SERVER */

export const requestDocDelete = (id, section) => {
    return {
        type: CONTENT_DELETE__REQUEST,
        section,
        id
    }
}

export const receiveDocDelete = (response) => {
    return {
        type: CONTENT_DELETE__SUCCESS,
        section: response.section,
        id: response.id
    }
}