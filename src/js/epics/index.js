import { combineEpics, createEpicMiddleware } from 'redux-observable'
import registerEpic from './register'
import authEpic from './auth'
import fetchEpic from './fetch'
import pushEpic from './push'
import addEpic from './add'
import deleteEpic from './delete'

// Merge the epics
const appEpic = combineEpics(
    registerEpic,
    authEpic,
    fetchEpic,
    pushEpic,
    addEpic,
    deleteEpic
)

const appEpicMiddleware = createEpicMiddleware(appEpic)

export default appEpicMiddleware