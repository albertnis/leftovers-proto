import 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'
import { REGISTER__REQUEST } from '../constants/actions'
import { receiveRegistration, registrationAjaxError } from '../actions' 
import { Observable } from 'rxjs/Observable';

// Asynchronously handle registration submission
const registerEpic = action$ => {
    return action$.ofType(REGISTER__REQUEST)
        .mergeMap(action => 
                ajax.post(`register?regcode=${action.regcode}`)
                .map(response => receiveRegistration(response.response))
                .catch(error => {
                    return Observable.of(registrationAjaxError(error))
                }))
}

export default registerEpic