import 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'
import { ADMIN_FETCH__REQUEST,
         CONTENT_DELETE__SUCCESS } from '../constants/actions'
import { receiveRegistration, registrationAjaxError, receiveAdminFetch } from '../actions' 
import { Observable } from 'rxjs/Observable';

// Asynchronously handle data fetch submission
const fetchEpic = action$ => {
    return action$.ofType(ADMIN_FETCH__REQUEST, CONTENT_DELETE__SUCCESS)
        .mergeMap(action => 
                ajax.post(`admin?action=fetch`)
                .map(response => receiveAdminFetch(response.response)))
}

export default fetchEpic