import 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'
import { CONTENT_DELETE__REQUEST } from '../constants/actions'
import { receiveDocDelete } from '../actions' 
import { Observable } from 'rxjs/Observable';

// Asynchronously handle entity deletion submission
const deleteEpic = action$ => {
    return action$.ofType(CONTENT_DELETE__REQUEST)
        .mergeMap(action => 
                ajax.post(`admin?action=delete&section=${action.section}&id=${JSON.stringify(action.id)}`)
                .map(response => receiveDocDelete(response.response)))
}

export default deleteEpic