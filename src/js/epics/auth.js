import 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'
import { LOGIN__REQUEST } from '../constants/actions'
import { receiveLogin, loginAjaxError } from '../actions' 
import { Observable } from 'rxjs/Observable';

// Asynchronously handle login submission
const authEpic = action$ => {
    return action$.ofType(LOGIN__REQUEST)
        .mergeMap(action => 
                ajax.post(`login?username=${action.username}&password=${action.password}`)
                .map(response => receiveLogin(response.response))
                .catch(error => {
                    return Observable.of(loginAjaxError(error))
                }))
}

export default authEpic