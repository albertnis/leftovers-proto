import 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'
import { CONTENT_PUSH__REQUEST } from '../constants/actions'
import { receiveDocSave } from '../actions' 
import { Observable } from 'rxjs/Observable';

// Asynchronously handle entity edit submission
const pushEpic = action$ => {
    return action$.ofType(CONTENT_PUSH__REQUEST)
        .mergeMap(action => 
                ajax.post(`admin?action=push&section=${action.section}&flatitem=${JSON.stringify(action.flatItem)}`)
                .map(response => receiveDocSave(response.response)))
}

export default pushEpic