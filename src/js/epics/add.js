import 'rxjs'
import { ajax } from 'rxjs/observable/dom/ajax'
import { CONTENT_ADD__REQUEST } from '../constants/actions'
import { receiveDocAdd } from '../actions' 
import { Observable } from 'rxjs/Observable';

// Asynchronously handle entity add submission
const addEpic = action$ => {
    return action$.ofType(CONTENT_ADD__REQUEST)
        .mergeMap(action => 
                ajax.post(`admin?action=add&section=${action.section}&flatitem=${JSON.stringify(action.flatItem)}`)
                .map(response => receiveDocAdd(response.response)))
}

export default addEpic