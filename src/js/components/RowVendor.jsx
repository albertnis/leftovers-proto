import React from 'react'
import ReactDOM from 'react-dom'
import EntityList from './EntityList.jsx'

class RowVendor extends React.Component {
    constructor(props) {
        super(props)
        this.setupState = this.setupState.bind(this)
        this.resetState = this.resetState.bind(this)
        this.setupState()

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.onCancel = this.onCancel.bind(this)
    }

    setupState() {
        /* TODO: Refactor to remove duplication between setupState() and resetState() */
        this.state = {
            name: this.props.vendor.name,
            address: this.props.vendor.address,
            instructions: this.props.vendor.instructions,
            longitude: this.props.vendor.location.coordinates[0],
            latitude: this.props.vendor.location.coordinates[1],
            reps: this.props.vendor.reps,
            _id: this.props.vendor._id
        }
    }

    resetState() {
        this.setState({
            name: this.props.vendor.name,
            address: this.props.vendor.address,
            instructions: this.props.vendor.instructions,
            longitude: this.props.vendor.location.coordinates[0],
            latitude: this.props.vendor.location.coordinates[1],
            reps: this.props.vendor.reps,
            _id: this.props.vendor._id
        })
    }

    onCancel() {
        
        this.resetState()
        this.props.onCancel()
    }

    handleInputChange(e) {
        /* TODO: Form validation and/or tools, especially coordinates */
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        /* TODO: Add 'waiting for server' feedback */
        e.preventDefault()
        this.props.onSave(this.state)
    }

    render() {
        var textopts = {}
        var checkopts = {}
        if (!this.props.editing) {
            textopts['readOnly'] = 'readOnly'
            checkopts['disabled'] = 'disabled'
        }
        return (
            <div className={`tableRow${this.props.editing ? ' tableRow-edit' : ''}`}>
                <form className={`tableRow-sub tableRow-inForm${this.props.editing ? ' tableRow-inForm-edit' : ''}`} onSubmit={this.handleSubmit} >
                    <input 
                        type="text" 
                        name="name" 
                        placeholder="Name"
                        value={this.state.name}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="address" 
                        placeholder="Address"
                        value={this.state.address}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="instructions" 
                        placeholder="Delivery instructions"
                        value={this.state.instructions}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="number" 
                        name="longitude" 
                        placeholder="Longitude"
                        min="-180"
                        max="180"
                        step="0.00001"
                        value={this.state.longitude}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="number" 
                        name="latitude" 
                        placeholder="Latitude"
                        min="-90"
                        step="0.00001"
                        max="90"
                        value={this.state.latitude}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <EntityList
                        name="reps"
                        placeholder="Add a rep"
                        onChange={this.handleInputChange}
                        value={this.state.reps}
                        entities={this.props.userEntities}
                        {...checkopts} />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-editButton"
                        value="Edit" 
                        onClick={() => this.props.onEdit()} />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-cancelButton"
                        value="Cancel"
                        onClick={this.onCancel} />
                    <input 
                        type="submit"
                        className="tableRow-inForm-button tableRow-inForm-saveButton" 
                        value="Save" />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-deleteButton"
                        value="Delete"
                        onClick={() => this.props.onDelete()} />
                </form>
            </div>
        )
    }
}

export default RowVendor