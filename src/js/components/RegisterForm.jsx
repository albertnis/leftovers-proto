import React from 'react'
import ReactDOM from 'react-dom'
import FormStatusIndicator from './FormStatusIndicator.jsx'

class RegisterForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            regcode: props.regcode
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(e) {
        // Control these inputs
        const value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.onSubmit(this.state.regcode)
    }

    render() {
        return (
            <div className="registerForm-wrapper">
                <FormStatusIndicator 
                    message={this.props.message} 
                    status={this.props.status} />
                <form className="registerForm" onSubmit={this.handleSubmit}>
                    <input 
                        className="registerForm-regcodeInput" 
                        type="text"
                        name="regcode" 
                        value={this.state.regcode} 
                        placeholder="Registration code"
                        onChange={this.handleInputChange} />
                    <input 
                        className="registerForm-submitButton" 
                        type="submit" 
                        value="Register" />
                </form>
            </div>
        )
    }
}

export default RegisterForm