import React from 'react'
import ReactDOM from 'react-dom'
import EntitySelector from './EntitySelector.jsx'

class EntityList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.onEntityRemove = this.onEntityRemove.bind(this)
        this.onEntityAdd = this.onEntityAdd.bind(this)
        this.onSelectChange = this.onSelectChange.bind(this)
    }

    onSelectChange(e) {
        // Control select component
        console.log(e.target.value)
        this.setState({
            selected: e.target.value
        }) 
    }

    onEntityRemove(id) {
        var newValue =  this.props.value.filter((testid) => {
            return testid !== id
        })
        this.handleChange(newValue)
    }

    onEntityAdd() {
        var justSelected = this.state.selected
        this.setState({
            selected: ''
        })
        var newValue = [justSelected, ...this.props.value]
        this.handleChange(newValue)
    }

    handleChange(newValue) {
        // Simulate the fields of an actual input as required by parent form
        var newe = {
            target: { 
                value: newValue,
                type: 'entitylist',
                name: this.props.name
            }
        }
        this.props.onChange(newe)
    }

    render() {
        // Work out which entities are present in the list...
        var matchedEntities = this.props.entities.filter((refEntity) => {
            return this.props.value.includes(refEntity._id)
        })

        // ... and work out which aren't (these will be allowed to be added)
        var unMatchedEntities = this.props.entities.filter((refEntity) => {
            return !this.props.value.includes(refEntity._id)
        })

        // Disallow adding placeholder entity
        var checkopts = {}
        if (this.state.selected == '') {
            checkopts['disabled'] = 'disabled'
        }

        return (
            <div className="entityList">
                <input
                    type="hidden" 
                    name={this.props.name}
                    value={this.props.value} />
                {matchedEntities.map((entity, index) => (
                    <div key={entity._id} className="entityList-item">
                        <div
                            className="entityList-item-content">{entity.value}</div>
                        <div 
                            title="Remove item" 
                            className="entityList-item-removeButton"
                            onClick={() => this.onEntityRemove(entity._id)}>&times;</div>
                    </div>
                ))}
                {!(this.props.disabled == "disabled") &&
                    <div className="entityList-item">
                        <EntitySelector
                            value={this.state.selected}
                            className="entityList-item-selector"
                            entities={unMatchedEntities}
                            placeholder={this.props.placeholder}
                            disabled={this.props.disabled}
                            onChange={this.onSelectChange} />
                        <div 
                            title="Add item" 
                            className="entityList-item-addButton"
                            onClick={this.onEntityAdd}
                            {...checkopts}>+</div>
                    </div>
                }
            </div>
        )
    }
}

export default EntityList