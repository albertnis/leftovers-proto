import React from 'react'
import ReactDOM from 'react-dom'

const FormStatusIndicator = ( props ) => (
    <div className={`registerStatus + ${props.status}`}>{props.message}</div>
)

export default FormStatusIndicator