import React from 'react'
import ReactDOM from 'react-dom'
import Header from './Header.jsx'
import LoginFormContainer from '../containers/LoginFormContainer.jsx'

const AppLogin = () => (
    <div className="wrapperAppLogin">
        <Header />
        <LoginFormContainer />
    </div>
)

export default AppLogin