import React from 'react'
import ReactDOM from 'react-dom'

class RowUser extends React.Component {
    constructor(props) {
        super(props)
        this.setupState = this.setupState.bind(this)
        this.resetState = this.resetState.bind(this)
        this.setupState()

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.onCancel = this.onCancel.bind(this)
    }

    setupState() {
        /* TODO: Refactor to remove duplication between setupState() and resetState() */
        var regSummary = this.props.user.registered ? this.props.user.registeredOn.toString() : '-'
        this.state = {
            firstname: this.props.user.firstname,
            lastname: this.props.user.lastname,
            fb: this.props.user.fb,
            regcode: this.props.user.regcode,
            addedOn: this.props.user.addedOn,
            regSummary,
            registered: this.props.user.registered,
            registeredOn: this.props.user.registeredOn,
            longitude: this.props.user.location.coordinates[0],
            latitude: this.props.user.location.coordinates[1],
            _id: this.props.user._id
        }
    }

    resetState() {
        var regSummary = this.props.user.registered ? this.props.user.registeredOn.toString() : '-'
        this.setState({
            firstname: this.props.user.firstname,
            lastname: this.props.user.lastname,
            fb: this.props.user.fb,
            regcode: this.props.user.regcode,
            addedOn: this.props.user.addedOn,
            regSummary,
            registered: this.props.user.registered,
            registeredOn: this.props.user.registeredOn,
            longitude: this.props.user.location.coordinates[0],
            latitude: this.props.user.location.coordinates[1],
            _id: this.props.user._id
        })
    }

    onCancel() {
        
        this.resetState()
        this.props.onCancel()
    }

    handleInputChange(e) {
        /* TODO: Form validation and/or tools, especially coordinates */
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        /* TODO: Add 'waiting for server' feedback */
        e.preventDefault()
        this.props.onSave(this.state)
    }

    render() {
        this.state.regSummary = this.props.user.registered ? this.props.user.registeredOn.toString() : '-'
        var textopts = {}
        var checkopts = {}
        if (!this.props.editing) {
            textopts['readOnly'] = 'readOnly'
            checkopts['disabled'] = 'disabled'
        }
        return (
            <div className={`tableRow${this.props.editing ? ' tableRow-edit' : ''}`}>
                <form className={`tableRow-sub tableRow-inForm${this.props.editing ? ' tableRow-inForm-edit' : ''}`} onSubmit={this.handleSubmit} >
                    <input 
                        type="text" 
                        name="firstname" 
                        placeholder="First name"
                        value={this.state.firstname}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="lastname" 
                        placeholder="Last name"
                        value={this.state.lastname}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="fb" 
                        placeholder="Facebook ID"
                        value={this.state.fb}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="regcode" 
                        placeholder="Registration code"
                        value={this.state.regcode}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="addedOn" 
                        readOnly
                        value={this.state.addedOn} />
                    <input 
                        type="checkbox" 
                        name="registered"
                        checked={this.state.registered ? 'checked' : ''} 
                        onChange={this.handleInputChange}
                        {...checkopts} />
                    <input 
                        type="text" 
                        name="regSummary" 
                        readOnly 
                        value={this.state.regSummary} />
                    <input 
                        type="number" 
                        name="longitude" 
                        placeholder="Longitude"
                        min="-180"
                        max="180"
                        step="0.00001"
                        value={this.state.longitude}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="number" 
                        name="latitude" 
                        placeholder="Latitude"
                        min="-90"
                        max="90"
                        step="0.00001"
                        value={this.state.latitude}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-editButton"
                        value="Edit" 
                        onClick={() => this.props.onEdit()} />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-cancelButton"
                        value="Cancel"
                        onClick={this.onCancel} />
                    <input 
                        type="submit"
                        className="tableRow-inForm-button tableRow-inForm-saveButton" 
                        value="Save" />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-deleteButton"
                        value="Delete"
                        onClick={() => this.props.onDelete()} />
                </form>
            </div>
        )
    }
}

export default RowUser