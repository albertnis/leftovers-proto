import React from 'react'
import ReactDOM from 'react-dom'
import RowFood from './RowFood.jsx'

class TableFood extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            newFood: {
                _id: 'new',
                name: '',
                imageUrl: '',
                serves: 0,
                addedOn: new Date(),
                location: {
                    type: 'Point',
                    coordinates: [-43.5221026, 172.5721128]
                },
                vendor: '',
                repUid: '',
                claimants: []
            }
        }
    }

    render() {
        return (
            <div className={`tableAdmin tableFood${this.props.editing ? ' tableAdmin-editing' : ''}${this.props.active ? ' tableAdmin-active' : ''}`}>
                <div className="tableRow tableRow-header">
                    <div className="tableRow-sub">
                        <div>Name</div>
                        <div>Image URL</div>
                        <div>Serves</div>
                        <div>Added</div>
                        <div>Longitude</div>
                        <div>Latitude</div>
                        <div>Vendor</div>
                        <div>Claimants</div>
                    </div>
                </div>
                {this.props.food.map((food, index) => (
                    <RowFood 
                        key={food._id} 
                        food={food}
                        editing={this.props.nav.editId == food._id}
                        vendorEntities={this.props.vendorEntities}
                        userEntities={this.props.userEntities}
                        onCancel={() => this.props.docEditExit()}
                        onEdit={() => this.props.docEditEnter(food._id)} 
                        onDelete={() => this.props.requestDocDelete(food._id, this.props.nav.section)}
                        onSave={(flatItem) => this.props.requestDocSave(flatItem, this.props.nav.section) }/>
                ))}
                {this.props.nav.new && 
                    <RowFood 
                        food={this.state.newFood}
                        editing={true}
                        onCancel={() => this.props.docRemoveNew()}
                        vendorEntities={this.props.vendorEntities}
                        userEntities={this.props.userEntities}
                        onEdit={() => this.props.docEditEnter(food._id)} 
                        onDelete={() => this.props.requestDocDelete(food._id, this.props.nav.section)}
                        onSave={(flatItem) => this.props.requestDocAdd(flatItem, this.props.nav.section) }/>
                }
                {!this.props.nav.new && this.props.nav.editId == null &&
                    <div 
                        className="tableRow tableRow-newButton"
                        onClick={() => this.props.docAddNew()}><span className="plusSign">+</span> New Food</div>
                }
            </div>
        )
    }
}

export default TableFood