import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import axios from 'axios'
import { BrowserRouter as Router, Route } from 'react-router-dom'

// File imports
import AppAdmin from './AppAdmin.jsx'
import AppRegister from './AppRegister.jsx'
import AppLogin from './AppLogin.jsx'

const Root = ({ store }) => (
    <Provider store={store}>
        <Router>
            <div>
                <Route exact path="/register" component={AppRegister}/>
                <Route exact path="/login" component={AppLogin}/>
                <Route exact path="/admin" component={AppAdmin}/>
            </div>
        </Router>
    </Provider>
)

export default Root