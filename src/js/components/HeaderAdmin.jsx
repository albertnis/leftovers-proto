import React from 'react'
import ReactDOM from 'react-dom'
import TabBarContainer from '../containers/TabBarContainer.jsx'

const HeaderAdmin = () => (
    <div className="headerAdmin">
        <div className="headerAdmin-title">Leftovers</div>
        <div className="headerAdmin-logout"><a href="/logout">Logout</a></div>
        <TabBarContainer />
    </div>
)

export default HeaderAdmin