import React from 'react'
import ReactDOM from 'react-dom'

const Header = () => (
    <div className="header">
        <div className="header-title"><h1>Leftovers</h1></div>
        <div className="header-subtitle"><h2>Reducing food waste together</h2></div>
    </div>
)

export default Header