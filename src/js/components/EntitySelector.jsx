import React from 'react'
import ReactDOM from 'react-dom'

class EntitySelector extends React.Component {
    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        this.props.onChange(e)
    }

    render() {
        return (
            <div className={`${this.props.className}-wrapper`}>
                <select className={this.props.className} name={this.props.name} disabled={this.props.disabled} onChange={this.handleChange}>
                {this.props.placeholder &&
                    <option value="" disabled hidden selected={this.props.value == "" ? "selected" : ""}>
                        {this.props.placeholder}
                    </option>
                }
                    {this.props.entities.map((entity, index) => (
                        <option key={index} value={entity._id} selected={this.props.value == entity._id ? "selected" : ""}>
                            {entity.value}
                        </option>
                    ))}
                </select>
            </div>
        )
    }
}

export default EntitySelector