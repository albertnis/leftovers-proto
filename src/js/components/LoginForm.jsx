import React from 'react'
import ReactDOM from 'react-dom'
import FormStatusIndicator from './FormStatusIndicator.jsx'

class LoginForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleInputChange(e) {
        // Control these inputs
        const value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.onSubmit(this.state.username, this.state.password)
    }

    render() {
        return (
            <div className="loginForm-wrapper">
                <FormStatusIndicator 
                    message={this.props.message} 
                    status={this.props.status} />
                <form className="loginForm" onSubmit={this.handleSubmit}>
                    <input 
                        className="loginForm-usernameInput" 
                        type="text"
                        name="username" 
                        value={this.state.username} 
                        placeholder="Username"
                        onChange={this.handleInputChange} />
                    <input 
                        className="loginForm-passwordInput" 
                        type="password"
                        name="password" 
                        value={this.state.password} 
                        placeholder="Password"
                        onChange={this.handleInputChange} />
                    <input 
                        type="hidden"
                        name="next" 
                        value="/admin" />
                    <input 
                        className="loginForm-submitButton" 
                        type="submit" 
                        value="Login" />
                </form>
            </div>
        )
    }
}

export default LoginForm