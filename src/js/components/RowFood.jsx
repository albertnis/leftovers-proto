import React from 'react'
import ReactDOM from 'react-dom'
import EntitySelector from './EntitySelector.jsx'
import EntityList from './EntityList.jsx'

class RowFood extends React.Component {
    constructor(props) {
        super(props)
        this.setupState = this.setupState.bind(this)
        this.resetState = this.resetState.bind(this)
        this.setupState()

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.onCancel = this.onCancel.bind(this)
    }

    setupState() {
        /* TODO: Refactor to remove duplication between setupState() and resetState() */
        this.state = {
            name: this.props.food.name,
            imageUrl: this.props.food.imageUrl,
            serves: this.props.food.serves,
            addedOn: this.props.food.addedOn,
            longitude: this.props.food.location.coordinates[0],
            latitude: this.props.food.location.coordinates[1],
            vendor: this.props.food.vendor,
            claimants: this.props.food.claimants,
            _id: this.props.food._id
        }
    }

    resetState() {
        this.setState({
            name: this.props.food.name,
            imageUrl: this.props.food.imageUrl,
            serves: this.props.food.serves,
            addedOn: this.props.food.addedOn,
            longitude: this.props.food.location.coordinates[0],
            latitude: this.props.food.location.coordinates[1],
            vendor: this.props.food.vendor,
            claimants: this.props.food.claimants,
            _id: this.props.food._id
        })
    }

    onCancel() {
        
        this.resetState()
        this.props.onCancel()
    }

    handleInputChange(e) {
        /* TODO: Form validation and/or tools, especially coordinates */
        /* TODO: Update coordinates to match vendor on vendor change */
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        /* TODO: Add 'waiting for server' feedback */
        e.preventDefault()
        this.props.onSave(this.state)
    }

    render() {
        var textopts = {}
        var checkopts = {}
        if (!this.props.editing) {
            textopts['readOnly'] = 'readOnly'
            checkopts['disabled'] = 'disabled'
        }
        return (
            <div className={`tableRow${this.props.editing ? ' tableRow-edit' : ''}`}>
                <form className={`tableRow-sub tableRow-inForm${this.props.editing ? ' tableRow-inForm-edit' : ''}`} onSubmit={this.handleSubmit} >
                    <input 
                        type="text" 
                        name="name" 
                        placeholder="Name"
                        value={this.state.name}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="imageUrl" 
                        placeholder="Image URL"
                        value={this.state.imageUrl}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="number" 
                        name="serves" 
                        placeholder="Serves"
                        min={0}
                        step={1}
                        value={this.state.serves}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="text" 
                        name="addedOn" 
                        readOnly
                        value={this.state.addedOn} />
                    <input 
                        type="number" 
                        name="longitude" 
                        placeholder="Longitude"
                        min="-180"
                        max="180"
                        step="0.00001"
                        value={this.state.longitude}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <input 
                        type="number" 
                        name="latitude" 
                        placeholder="Latitude"
                        min="-90"
                        max="90"
                        step="0.00001"
                        value={this.state.latitude}
                        onChange={this.handleInputChange}
                        {...textopts} />
                    <EntitySelector 
                        name="vendor"
                        placeholder="Select a vendor"
                        className="entitySelector"
                        value={this.state.vendor}
                        onChange={this.handleInputChange}
                        entities={this.props.vendorEntities}
                        {...checkopts} />
                    <EntityList
                        name="claimants"
                        placeholder="Add a claimant"
                        onChange={this.handleInputChange}
                        value={this.state.claimants}
                        entities={this.props.userEntities}
                        {...checkopts} />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-editButton"
                        value="Edit" 
                        onClick={() => this.props.onEdit()} />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-cancelButton"
                        value="Cancel"
                        onClick={this.onCancel} />
                    <input 
                        type="submit"
                        className="tableRow-inForm-button tableRow-inForm-saveButton" 
                        value="Save" />
                    <input 
                        type="button"
                        className="tableRow-inForm-button tableRow-inForm-deleteButton"
                        value="Delete"
                        onClick={() => this.props.onDelete()} />
                </form>
            </div>
        )
    }
}

export default RowFood