import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import HeaderAdmin from './HeaderAdmin.jsx'

import TableUserContainer from '../containers/TableUserContainer.jsx'
import TableVendorContainer from '../containers/TableVendorContainer.jsx'
import TableFoodContainer from '../containers/TableFoodContainer.jsx'
import { requestAdminFetch } from '../actions'

class AppAdmin extends React.Component {
    componentDidMount() {
        /* TODO: Loading indicator while waiting for fetch results */
        this.props.requestAdminFetch()
    }

    render() {
        return (
            <div className="wrapperPage">
                <HeaderAdmin />
                <TableUserContainer />
                <TableVendorContainer />
                <TableFoodContainer />
            </div>
        )
    }
}

// Connect to redux

const mapStateToProps = (state) => ({})

const mapDispatchToProps = {
    requestAdminFetch
}

export default connect(mapStateToProps, mapDispatchToProps)(AppAdmin)