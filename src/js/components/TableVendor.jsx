import React from 'react'
import ReactDOM from 'react-dom'
import RowVendor from './RowVendor.jsx'

class TableVendor extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            newVendor: {
                _id: 'new',
                name: '',
                address: '',
                instructions: '',
                addedOn: new Date(),
                location: {
                    type: 'Point',
                    coordinates: [-43.5221026, 172.5721128]
                },
                reps: []
            }
        }
    }

    render() {
        return (
            <div className={`tableAdmin tableVendor${this.props.editing ? ' tableAdmin-editing' : ''}${this.props.active ? ' tableAdmin-active' : ''}`}>
                <div className="tableRow tableRow-header">
                    <div className="tableRow-sub">
                        <div>Name</div>
                        <div>Address</div>
                        <div>Instructions</div>
                        <div>Longitude</div>
                        <div>Latitude</div>
                        <div>Representatives</div>
                    </div>
                </div>
                {this.props.vendors.map((vendor, index) => (
                    <RowVendor 
                        key={vendor._id} 
                        vendor={vendor}
                        editing={this.props.nav.editId == vendor._id}
                        userEntities={this.props.userEntities}
                        onCancel={() => this.props.docEditExit()}
                        onEdit={() => this.props.docEditEnter(vendor._id)} 
                        onDelete={() => this.props.requestDocDelete(vendor._id, this.props.nav.section)}
                        onSave={(flatItem) => this.props.requestDocSave(flatItem, this.props.nav.section) }/>
                ))}
                {this.props.nav.new && 
                    <RowVendor 
                        vendor={this.state.newVendor}
                        editing={true}
                        onCancel={() => this.props.docRemoveNew()}
                        userEntities={this.props.userEntities}
                        onEdit={() => this.props.docEditEnter(vendor._id)} 
                        onDelete={() => this.props.requestDocDelete(vendor._id, this.props.nav.section)}
                        onSave={(flatItem) => this.props.requestDocAdd(flatItem, this.props.nav.section) }/>
                }
                {!this.props.nav.new && this.props.nav.editId == null &&
                    <div 
                        className="tableRow tableRow-newButton"
                        onClick={() => this.props.docAddNew()}><span className="plusSign">+</span> New Vendor</div>
                }
            </div>
        )
    }
}

export default TableVendor