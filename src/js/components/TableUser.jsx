import React from 'react'
import ReactDOM from 'react-dom'
import RowUser from './RowUser.jsx'

class TableUser extends React.Component {
    constructor(props) {
        super(props)
        this.getRegCode = this.getRegCode.bind(this)
        this.state = {
            newUser: {
                _id: 'new',
                firstname: '',
                lastname: '',
                fb: '',
                regcode: this.getRegCode(),
                registered: false,
                addedOn: new Date(),
                registeredOn: new Date(),
                location: {
                    type: 'Point',
                    coordinates: [-43.5221026, 172.5721128]
                }
            }
        }
    }

    getRegCode() {
        var s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var rancode = Array(6).join().split(',').map(function() { return s.charAt(Math.floor(Math.random() * s.length)); }).join('');
        return rancode.toUpperCase()
    }

    render() {
        this.state.newUser.regcode = this.getRegCode()
        return (
            <div className={`tableAdmin tableUser${this.props.editing ? ' tableAdmin-editing' : ''}${this.props.active ? ' tableAdmin-active' : ''}`}>
                <div className="tableRow tableRow-header">
                    <div className="tableRow-sub">
                        <div>First name</div>
                        <div>Last name</div>
                        <div>Facebook ID</div>
                        <div>Registration code</div>
                        <div>Added</div>
                        <div>Registered</div>
                        <div>Registered On</div>
                        <div>Longitude</div>
                        <div>Latitude</div>
                    </div>
                </div>
                {this.props.users.map((user, index) => (
                    <RowUser 
                        key={user._id} 
                        user={user}
                        editing={this.props.nav.editId == user._id}
                        onCancel={() => this.props.docEditExit()}
                        onEdit={() => this.props.docEditEnter(user._id)} 
                        onDelete={() => this.props.requestDocDelete(user._id, this.props.nav.section)}
                        onSave={(flatItem) => this.props.requestDocSave(flatItem, this.props.nav.section) }/>
                ))}
                {this.props.nav.new && 
                    <RowUser 
                        user={this.state.newUser}
                        editing={true}
                        onCancel={() => this.props.docRemoveNew()}
                        onEdit={() => this.props.docEditEnter(user._id)} 
                        onDelete={() => this.props.requestDocDelete(user._id, this.props.nav.section)}
                        onSave={(flatItem) => this.props.requestDocAdd(flatItem, this.props.nav.section) }/>
                }
                {!this.props.nav.new && this.props.nav.editId == null &&
                    <div 
                        className="tableRow tableRow-newButton"
                        onClick={() => this.props.docAddNew()}><span className="plusSign">+</span> New User</div>
                }
            </div>
        )
    }
}

export default TableUser