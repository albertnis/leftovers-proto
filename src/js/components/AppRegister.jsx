import React from 'react'
import ReactDOM from 'react-dom'
import Header from './Header.jsx'
import RegisterFormContainer from '../containers/RegisterFormContainer.jsx'

const AppRegister = () => (
    <div className="wrapperAppRegister">
        <Header />
        <RegisterFormContainer />
    </div>
)

export default AppRegister