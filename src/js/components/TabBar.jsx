import React from 'react'
import ReactDOM from 'react-dom'
import FormStatusIndicator from './FormStatusIndicator.jsx'
import { NAV_SECTION_USER,
         NAV_SECTION_VENDOR,
         NAV_SECTION_FOOD } from '../constants'

class TabBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            section: NAV_SECTION_USER
        }
        this.onTabSelect = this.onTabSelect.bind(this)
        this.onUnload = this.onUnload.bind(this)
    }

    onTabSelect(sectionSelected) {
        if (!this.props.editing) {
            this.setState({
                section: sectionSelected
            })
            this.props.onChange(sectionSelected)
        }
    }

    onUnload(e) {
        // Not working
        /* TODO: Work out how to properly implement onUnload behaviour */
        var display = 'Are you sure? Unsaved changes will be lost if you continue.'
        e.returnValue = display
        return display
    }

    render() {
        /* TODO: Implement routes (one per tab) for more predicatable page refresh UX */
        return (
            <ul className={`tabBar${this.props.editing ? ' tabBar-editing' : ''}`}>
                <li className={`tabBar-link${this.state.section == NAV_SECTION_USER ? ' tabBar-link_active' : ''}`}
                    onClick={() => this.onTabSelect(NAV_SECTION_USER)}>Users</li>
                <li className={`tabBar-link${this.state.section == NAV_SECTION_VENDOR ? ' tabBar-link_active' : ''}`}
                onClick={() => this.onTabSelect(NAV_SECTION_VENDOR)}>Vendors</li><li className={`tabBar-link${this.state.section == NAV_SECTION_FOOD ? ' tabBar-link_active' : ''}`}
                onClick={() => this.onTabSelect(NAV_SECTION_FOOD)}>Food</li>
            </ul>
        )
    }
}

export default TabBar