import { connect } from 'react-redux'
import TableVendor from '../components/TableVendor.jsx'
import { NAV_SECTION_VENDOR } from '../constants'
import { docEditEnter, 
    docEditExit, 
    requestDocSave, 
    docRemoveNew,
    docAddNew,
    requestDocAdd,
    requestDocDelete } from '../actions'

const mapStateToProps = (state) => {
    var userEntities = state.users.map( (user, index) => {
        return {
            _id: user._id,
            value: `${user.firstname} ${user.lastname}`
        }
    })
    return {
        vendors: state.vendors,
        nav: state.nav,
        editing: state.nav.editId !== null,
        active: state.nav.section == NAV_SECTION_VENDOR,
        userEntities
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        docEditEnter: (id) => {
            dispatch(docEditEnter(id))
        },
        docEditExit: () => {
            dispatch(docEditExit())
        },
        requestDocSave: (flatItem, section) => {
            dispatch(requestDocSave(flatItem, section))
        },
        docRemoveNew: () => {
            dispatch(docEditExit())
            dispatch(docRemoveNew())
        },
        docAddNew: () => {
            dispatch(docEditEnter('new'))
            dispatch(docAddNew())
        },
        requestDocAdd: (flatItem, section) => {
            dispatch(requestDocAdd(flatItem, section))
        },
        requestDocDelete: (id, section) => {
            dispatch(requestDocDelete(id, section))
        }
    }
}

const TableVendorContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TableVendor)

export default TableVendorContainer