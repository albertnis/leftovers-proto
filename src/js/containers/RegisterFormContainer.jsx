import { connect } from 'react-redux'
import { requestRegistration } from '../actions'
import RegisterForm from '../components/RegisterForm.jsx'

const mapStateToProps = (state) => {
    console.log(state.register)
    return {
        regcode: state.register.regcode,
        status: state.register.status,
        message: state.register.message
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (regcode) => {
            dispatch(requestRegistration(regcode))
        }
    }
}

const RegisterFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterForm)

export default RegisterFormContainer
