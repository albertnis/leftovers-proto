import { connect } from 'react-redux'
import { changeSection } from '../actions'
import TabBar from '../components/TabBar.jsx'

const mapStateToProps = (state) => {
    return {
        section: state.nav.section,
        editing: state.nav.editId !== null
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onChange: (section) => {
            dispatch(changeSection(section))
        }
    }
}

const TabBarContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TabBar)

export default TabBarContainer
