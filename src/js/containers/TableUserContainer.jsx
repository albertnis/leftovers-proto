import { connect } from 'react-redux'
import TableUser from '../components/TableUser.jsx'
import { NAV_SECTION_USER } from '../constants'
import { docEditEnter, 
    docEditExit, 
    requestDocSave, 
    docRemoveNew,
    docAddNew,
    requestDocAdd,
    requestDocDelete } from '../actions'

const mapStateToProps = (state) => {
    return {
        users: state.users,
        nav: state.nav,
        editing: state.nav.editId !== null,
        active: state.nav.section == NAV_SECTION_USER
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        docEditEnter: (id) => {
            dispatch(docEditEnter(id))
        },
        docEditExit: () => {
            dispatch(docEditExit())
        },
        requestDocSave: (flatItem, section) => {
            dispatch(requestDocSave(flatItem, section))
        },
        docRemoveNew: () => {
            dispatch(docEditExit())
            dispatch(docRemoveNew())
        },
        docAddNew: () => {
            dispatch(docEditEnter('new'))
            dispatch(docAddNew())
        },
        requestDocAdd: (flatItem, section) => {
            dispatch(requestDocAdd(flatItem, section))
        },
        requestDocDelete: (id, section) => {
            dispatch(requestDocDelete(id, section))
        }
    }
}

const TableUserContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TableUser)

export default TableUserContainer