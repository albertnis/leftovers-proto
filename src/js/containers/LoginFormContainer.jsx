import { connect } from 'react-redux'
import { requestLogin } from '../actions'
import LoginForm from '../components/LoginForm.jsx'

const mapStateToProps = (state) => {
    console.log(state.auth)
    return {
        status: state.auth.status,
        message: state.auth.message
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: (username, password) => {
            dispatch(requestLogin(username, password))
        }
    }
}

const LoginFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginForm)

export default LoginFormContainer
