import { connect } from 'react-redux'
import TableFood from '../components/TableFood.jsx'
import { NAV_SECTION_FOOD } from '../constants'
import { docEditEnter, 
    docEditExit, 
    requestDocSave, 
    docRemoveNew,
    docAddNew,
    requestDocAdd,
    requestDocDelete } from '../actions'

const mapStateToProps = (state) => {
    var vendorEntities = state.vendors.map( (vendor, index) => {
        return {
            _id: vendor._id,
            value: vendor.name
        }
    })
    var userEntities = state.users.map( (user, index) => {
        return {
            _id: user._id,
            value: `${user.firstname} ${user.lastname}`
        }
    })
    return {
        food: state.food,
        nav: state.nav,
        editing: state.nav.editId !== null,
        active: state.nav.section == NAV_SECTION_FOOD,
        vendorEntities,
        userEntities
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        docEditEnter: (id) => {
            dispatch(docEditEnter(id))
        },
        docEditExit: () => {
            dispatch(docEditExit())
        },
        requestDocSave: (flatItem, section) => {
            dispatch(requestDocSave(flatItem, section))
        },
        docRemoveNew: () => {
            dispatch(docEditExit())
            dispatch(docRemoveNew())
        },
        docAddNew: () => {
            dispatch(docEditEnter('new'))
            dispatch(docAddNew())
        },
        requestDocAdd: (flatItem, section) => {
            dispatch(requestDocAdd(flatItem, section))
        },
        requestDocDelete: (id, section) => {
            dispatch(requestDocDelete(id, section))
        }
    }
}

const TableFoodContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TableFood)

export default TableFoodContainer