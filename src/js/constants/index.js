// Misc constants
/* TODO: Demerge constants and class names where applicable */

export const REGISTER_STATUS_HIDE = 'registerStatus_hide'
export const REGISTER_STATUS_NEUTRAL = 'registerStatus_neutral'
export const REGISTER_STATUS_SUCCESS = 'registerStatus_success'
export const REGISTER_STATUS_ERROR = 'registerStatus_error'
export const REGISTER_STATUS_LOADING = 'registerStatus_loading'

export const LOGIN_STATUS_HIDE = 'loginStatus_hide'
export const LOGIN_STATUS_NEUTRAL = 'loginStatus_neutral'
export const LOGIN_STATUS_SUCCESS = 'loginStatus_success'
export const LOGIN_STATUS_ERROR = 'loginStatus_error'
export const LOGIN_STATUS_LOADING = 'loginStatus_loading'

export const NAV_SECTION_USER = 'users'
export const NAV_SECTION_VENDOR = 'vendors'
export const NAV_SECTION_FOOD = 'foods'

export const DOC_STATUS_SAVING = 'row-saving'
export const DOC_STATUS_ERROR = 'row-error'
export const DOC_STATUS_SUCCESS = 'row-success'