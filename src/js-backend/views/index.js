import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { renderToString } from 'react-dom/server'

import appReducer from '../../js/reducers/index.js'
import Root from '../components/Root.jsx'

import { REGISTER_STATUS_NEUTRAL,
         LOGIN_STATUS_NEUTRAL,
         NAV_SECTION_USER } from '../../js/constants'

/* PAGE BASE HTML */

const renderFullPage = (html, preloadedState) => {
    // Server-rendered React app and base state passed in
    // This will be hydrated by client-side app (/js/front.js) when it loads
    return `<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Leftovers</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900|Didact+Gothic|PT+Sans|Montserrat:400,700|EB+Garamond:400" rel="stylesheet">
        <link href="public/main.bundle.css" rel="stylesheet">
        <link href="public/favicon.png" rel="icon">
    </head>
    <body>
        <div id="root">${html}</div>
        <script>
        window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
        </script>
        <script src="public/front-output.js"></script>
    </body>
</html>`
}

/* INITIALISATION FUNCTIONS */

const serveToFront = (initialState,req,res) => {

    // Turn initialState object into redux store
    const store = createStore(appReducer, initialState)

    // Do the render, making sure to use the server-side <Root /> component as imported
    const html = renderToString(
        <Root store={store} req={req} />
    )

    // Turn the state back into an object for injection
    const preloadedState = store.getState()

    // Send the rendered HTML and state as injected into the base HTML
    res.send(renderFullPage(html, preloadedState))
}

const makeInitialState = (req) => {

    // Get authentication status
    var auth_loggedIn = req.isAuthenticated()
    var auth_username = req.isAuthenticated() ? req.user.local.username : null
    
    // Fill state template accordingly
    // NB: users, vendors and food will be populated later via AJAX
    var initialState = {
        register: {
            status: REGISTER_STATUS_NEUTRAL,
            message: "Please enter code below.",
            regcode: (typeof(req.query.code) === "string") ? req.query.code : ""
        },
        auth: {
            status: LOGIN_STATUS_NEUTRAL,
            message: 'Enter username and password to log in.'
        },
        nav: {
            section: NAV_SECTION_USER,
            editId:null,
            new: false
        },
        users: [],
        vendors: [],
        food: []
    }

    return initialState
}

export const handleRender = (req, res) => {
    // Make the initial state, then use it to render app server-side
    serveToFront(makeInitialState(req),req,res)
}