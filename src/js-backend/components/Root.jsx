import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import axios from 'axios'
import { StaticRouter as Router, Route } from 'react-router-dom'

// File imports
import AppAdmin from '../../js/components/AppAdmin.jsx'
import AppLogin from '../../js/components/AppLogin.jsx'
import AppRegister from '../../js/components/AppRegister.jsx'

var context = {}

const Root = ({ store, req }) => (
    <Provider store={store} context={context}>
        <Router location={req.url} >
            <div>
                <Route exact path="/register" component={AppRegister}/>
                <Route exact path="/login" component={AppLogin}/>
                <Route exact path="/admin" component={AppAdmin}/>
            </div>
        </Router>
    </Provider>
)

export default Root