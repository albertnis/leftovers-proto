/* IMPORT and SETUP */
// Server essentials
var path = require('path')
var Express = require('express')
const app = Express()
var session = require('express-session')
var Morgan = require('morgan')
var flash = require('connect-flash')
var CookieParser = require('cookie-parser')
var BodyParser = require('body-parser')

// Authentication setup
var passport = require('passport')
import pConfig from './config/passport'
pConfig(passport)

// File imports
import { handleRender, handleLogin } from './views'
import { handleAdmin, handleRegister } from './db/tools'

// Database setup
const mongoose = require('mongoose')
var dbAddress = 'mongodb://127.0.0.1/leftovers'
mongoose.connect(dbAddress, {
    useMongoClient: false
})
mongoose.Promise = global.Promise
var db = mongoose.connection
db.on('error', console.error.bind(console, 'Database connection error:'));

// App (express) setup
const port = 3000
app.use(Morgan('dev'))
app.use(CookieParser('foo'))
app.use(BodyParser())
app.use(session({ secret:'foo', cookie: {maxAge: 24*60*60*1000} }))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

/* REQUEST HANDLING */
// A login request from the form
app.post('/login', function(req, res, next) {
    // Attempt login
    passport.authenticate('local-login', function(err, user, info) {
        req.logIn(user, function (err) {
            return res.send(info)
        });
    })(req, res, next)
})

// Login page load
app.get('/login', function(req, res, next) {
    if (!req.isAuthenticated()) {
        // Serve React app
        handleRender(req, res, next)
    }
    else {
        // Already logged in, so redirect straight to admin page
        res.redirect('/admin')
    }
})

// Register page load
app.get('/register', handleRender)

// Register request from the form
app.post('/register', handleRegister)

// Admin page load
app.get('/admin', function(req, res, next) {
    if (req.isAuthenticated()) {
        // Logged in, so serve React app
        handleRender(req, res, next)
    }
    else {
        // Not logged in, so deny access and redirect to login page
        res.redirect('/login')
    }
})

// Admin action requested from app
app.post('/admin', function(req, res, next) {
    if (req.isAuthenticated()) {
        // Logged in, so handle this request
        handleAdmin(req, res, next)
    }
    else {
        // Not logged in, so deny access and serve 401 Not Authenticated error
        res.status(401).send('Not logged in')
    }
})

// Logout 'page' load
app.get('/logout', function(req, res, next) {
    // There is no logout page, so just de-authenticate and redirect back to login
    req.logOut()
    res.redirect('/login')
})

/* TODO: Hook the bot in here-ish */

// File in the /public directory are served statically as desired
app.use('/public', Express.static('public'))

// Log that server is up
console.log('Listening on port', port)

// Spin up server
app.listen(port)