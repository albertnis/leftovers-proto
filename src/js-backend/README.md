# Backend JavaScript

Here lies the backend Javascript for the application. 

Entry point: `back.js`.

Folders:

- **Components**: Contains server-side `Root.jsx` which renders remaining components from frontend files.
- **Config**: Contains library setup, notably authentication routines for passport.
- **DB**: Miscellaneous database logic, including admin tasks.
- **Models**: Mongoose schema definitions for database objects.
- **Views**: Entry point for server-side rendering. Contains base app HTML.