var User = require('../models/user');
var Vendor = require('../models/vendor');
var Food = require('../models/food')
import { NAV_SECTION_USER,
         NAV_SECTION_VENDOR,
         NAV_SECTION_FOOD } from '../../js/constants'

export const handleDelete = (req, res, next) => {
    // Form will pass id of item to be deleted

    // Extract parsed data
    var id = JSON.parse(req.query.id)

    // Determine section based on query
    switch(req.query.section) {
        case NAV_SECTION_FOOD:

            // Remove item based on passed id
            Food.findByIdAndRemove(id, function(err) {
                if (err) { 
                    // Server error
                    console.log(err)
                }
                else {
                    // Success! Bounce back id removed and provided section
                    console.log('Food removed')
                    res.send(JSON.stringify({id, section: req.query.section}))
                }
            })
            break
        case NAV_SECTION_VENDOR:
            Vendor.findByIdAndRemove(id, function(err) {
                if (err) { 
                    console.log(err)
                }
                else {
                    console.log('Vendor removed')

                    // PROPOGATION:
                    // Remove food item which were listed by that vendor
                    Food.deleteMany({vendor: id}, function(err) {
                        if (err) throw err
                        console.log('Vendor food offerings removed')

                        // All propogation completed, time to respond
                        res.send(JSON.stringify({id, section: req.query.section}))
                    })
                }
            })
            break
        case NAV_SECTION_USER:
        default:
            /* TODO: Implement a smarter default clause in case invalid section provided */
            
            User.findByIdAndRemove(id, function(err) {
                if (err) { 
                    console.log(err)
                }
                else {
                    console.log('User removed')

                    // PROPOGATION:
                    // Remove vendor rep entries matching that user...
                    Vendor.update({reps: id}, {
                        $pull: { reps: id }
                    }, {multi: true}, function(err) {
                        if (err) throw err
                        console.log('User removed from companies')

                        // ...and also remove food claims from that user
                        Food.update({claimants: id}, {
                            $pull: { claimants: id }
                        }, {multi: true}, function(err) {
                            if (err) throw err
                            // We're in !!!CaLLbAcK hEll!!! //
                            /* TODO: Get out of callback hell */
                            console.log('User removed from food claims')

                            // All propogation completed, time to respond
                            res.send(JSON.stringify({id, section: req.query.section}))
                        })
                    })
                    
                }
            })
    }
}

export default handleDelete