var User = require('../models/user');
import { NAV_SECTION_USER,
         NAV_SECTION_VENDOR,
         NAV_SECTION_FOOD,
         REGISTER_STATUS_SUCCESS,
         REGISTER_STATUS_ERROR } from '../../js/constants'
import handleFetch from './handleFetch'         
import handlePush from './handlePush'
import handleAdd from './handleAdd'
import handleDelete from './handleDelete'

export const handleAdmin = (req, res, next) => {
    switch (req.query.action) {
        case 'fetch':
            // The page has asked for a fresh copy of the database
            return handleFetch(req, res, next)
        case 'push':
            // A change save request has been submitted
            return handlePush(req, res, next)
        case 'add':
            // A new item save request has been submitted
            return handleAdd(req, res, next)
        case 'delete':
            // A delete request has been submitted
            return handleDelete(req, res, next)
    }
}

export const handleRegister = (req, res, next) => {
    // A registration request has been submitted

    if ((typeof(req.query.regcode) === 'undefined') || (req.query.regcode == '')) {
        // No valid registration code
        res.send(JSON.stringify({
            status: REGISTER_STATUS_ERROR, 
            message: 'No registration code provided'
        }))
    }
    else {
        // Update user registration date
        User.findOneAndUpdate({
            regcode: req.query.regcode,
            registered: false
        },{
            $set: {
                registered: true,
                registeredOn: new Date()
            }
        }, {new: true}, function(err, result) {
            if (err) { 
                // Database error
                res.send(JSON.stringify({
                    status: REGISTER_STATUS_ERROR, 
                    message: 'A database error occurred'
                }))
            }

            if (!result) {
                // No matching unregistered user found (bad regcode or already registered)
                res.send(JSON.stringify({
                    status: REGISTER_STATUS_ERROR, 
                    message: 'Invalid registration code provided'
                }))
            }

            else {
                // Success!
                res.send(JSON.stringify({
                    status: REGISTER_STATUS_SUCCESS, 
                    message: `Thanks for registering, ${result.firstname}!`
                }))
            }
        })
    }
}