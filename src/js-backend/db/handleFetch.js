var User = require('../models/user');
var Vendor = require('../models/vendor');
var Food = require('../models/food')
import { NAV_SECTION_USER,
         NAV_SECTION_VENDOR,
         NAV_SECTION_FOOD } from '../../js/constants'

const handleFetch = (req, res, next) => {
    // Query passes no parameters

    // Initialise returned arrays
    // These will be filled by database calls
    var allUsers = []
    var allVendors = []
    var allFood = []

    // Find all USERS...
    User.find({}, function(err, userResult) {
        if (err) { 
            // Database error
            console.log(err)
        }
        else {
            // Fill users
            allUsers = Object.assign([], userResult)

            // ...then find all VENDORS...
            Vendor.find({}, function(err, vendorResult) {
                if (err) { 
                    console.log(err)
                }
                else {
                    // Fill vendors
                    allVendors = Object.assign([], vendorResult)

                    // ...finally find all FOOD
                    Food.find({}, function(err, foodResult) {
                        if (err) { 
                            console.log(err)
                        }
                        else {
                            // We're in !!!CaLLbAcK hEll!!! //
                            /* TODO: Get out of callback hell */

                            // Fill food
                            allFood = Object.assign([], foodResult)

                            // Time to respond with this gathered data
                            res.send(JSON.stringify({users: allUsers, vendors: allVendors, food: allFood}))
                        }
                    })
                }
            })
        }
    })
}

export default handleFetch