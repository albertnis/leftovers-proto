var User = require('../models/user');
var Vendor = require('../models/vendor');
var Food = require('../models/food')
import { NAV_SECTION_USER,
         NAV_SECTION_VENDOR,
         NAV_SECTION_FOOD } from '../../js/constants'

const handleAdd = (req, res, next) => {
    // Form will pass new flattened document with no valid _id

    // Extract passed data
    var flatitem = JSON.parse(req.query.flatitem)

    // Determine section based on query
    switch(req.query.section) {
        case NAV_SECTION_FOOD:

            // Shape a database object for insertion, from flattened data
            var newFood = Food({
                name: flatitem.name,
                imageUrl: flatitem.imageUrl,
                serves: flatitem.serves,
                addedOn: flatitem.addedOn,
                location: {
                    type: 'Point',
                    coordinates: [flatitem.longitude, flatitem.latitude]
                },
                vendor: flatitem.vendor,
                claimants: flatitem.claimants
            })

            // Insert this document (MongoDB will auto-generate a valid ObjectID)
            newFood.save(function(err, doc) {
                if (err) { 
                    // Database error
                    console.log(err)
                }
                else {
                    // Success! Return added item (with auto-generated id) for updating frontend
                    console.log('Food updated', doc)
                    res.send(JSON.stringify({item: doc, section: req.query.section}))
                }
            })
            
            break
        case NAV_SECTION_VENDOR:
            var newVendor = Vendor({
                name: flatitem.name,
                address: flatitem.address,
                instructions: flatitem.instructions,
                addedOn: new Date(),
                location: {
                    type: 'Point',
                    coordinates: [flatitem.longitude, flatitem.latitude]
                },
                reps: flatitem.reps
            })
            newVendor.save(function(err, doc) {
                if (err) { 
                    console.log(err)
                }
                else {
                    console.log('Vendor updated', doc)
                    res.send(JSON.stringify({item: doc, section: req.query.section}))
                }
            })
            break
        case NAV_SECTION_USER:
        default:
            /* TODO: Implement a smarter default clause in case invalid section provided */
            var newUser = User({
                firstname: flatitem.firstname,
                lastname: flatitem.lastname,
                fb: flatitem.fb,
                regcode: flatitem.regcode,
                registered: flatitem.registered,
                registeredOn: flatitem.registered ? new Date() : null,
                addedOn: new Date(),
                location: {
                    type: 'Point',
                    coordinates: [flatitem.longitude, flatitem.latitude]
                }
            })
            newUser.save(function(err, doc) {
                if (err) { 
                    console.log(err)
                }
                else {
                    res.send(JSON.stringify({item: doc, section: req.query.section}))
                }
            })
    }
}

export default handleAdd