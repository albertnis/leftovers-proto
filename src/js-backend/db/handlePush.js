var User = require('../models/user');
var Vendor = require('../models/vendor');
var Food = require('../models/food')
import { NAV_SECTION_USER,
         NAV_SECTION_VENDOR,
         NAV_SECTION_FOOD } from '../../js/constants'

const genRegisterDate = (newRegisteredOn, oldRegisteredOn, newRegistered, oldRegistered) => {
    // A bit of logic to determine how registeration has changed
    
    if (newRegistered && !oldRegistered) {
        // Newly registered
        return new Date()
    }
    if (!newRegistered && oldRegistered) {
        // Newly deregistered
        return null
    }
    // No change
    return oldRegisteredOn
}

const handlePush = (req, res, next) => {
    // Form will pass flattened document with all fields and valid _id

    // Extract passed data
    var flatitem = JSON.parse(req.query.flatitem)
    
    // Determine section based on query
    switch(req.query.section) {
        case NAV_SECTION_FOOD:

            // Ask mongoose to find the item and set fields to new values
            Food.findByIdAndUpdate(flatitem._id, {
                $set: {
                    name: flatitem.name,
                    imageUrl: flatitem.imageUrl,
                    serves: flatitem.serves,
                    addedOn: flatitem.addedOn,
                    location: {
                        coordinates: [flatitem.longitude, flatitem.latitude]
                    },
                    vendor: flatitem.vendor,
                    claimants: flatitem.claimants
                }
            }, {new: true}, function(err, doc) {
                if (err) { 
                    // Database error
                    console.log(err)
                }
                else {
                    console.log('Food updated', doc)
                    
                    // Success! Return added item (with auto-generated id) for updating frontend
                    res.send(JSON.stringify({item: doc, section: req.query.section}))
                }
            })
            break
        case NAV_SECTION_VENDOR: 
            Vendor.findByIdAndUpdate(flatitem._id, {
                $set: {
                    name: flatitem.name,
                    address: flatitem.address,
                    instructions: flatitem.instructions,
                    addedOn: flatitem.addedOn,
                    location: {
                        coordinates: [flatitem.longitude, flatitem.latitude]
                    },
                    reps: flatitem.reps
                }
            }, {new: true}, function(err, doc) {
                if (err) { 
                    console.log(err)
                }
                else {
                    console.log('Vendor updated', doc)
                    res.send(JSON.stringify({item: doc, section: req.query.section}))
                }
            })
            break        
        case NAV_SECTION_USER:
        default:
            /* TODO: Implement a smarter default clause in case invalid section provided */

            // Find the user before updating...
            User.findById(flatitem._id, function(err,doc) {
                if (err) throw err
                
                // ...in order to work out how registration is going to change
                var newRegDate = genRegisterDate(flatitem.registeredOn,doc.registeredOn,
                                                    flatitem.registered, doc.registered)

                // Now good to go as before
                User.findByIdAndUpdate(flatitem._id, {
                    $set: {
                        firstname: flatitem.firstname,
                        lastname: flatitem.lastname,
                        fb: flatitem.fb,
                        regcode: flatitem.regcode,
                        registered: flatitem.registered,
                        registeredOn: newRegDate,
                        location: {
                            coordinates: [flatitem.longitude, flatitem.latitude]
                        }
                    }
                }, {new: true}, function(err, doc) {
                    if (err) { 
                        console.log(err)
                    }
                    else {
                        console.log('User updated', doc)
                        res.send(JSON.stringify({item: doc, section: req.query.section}))
                    }
                })
            })
            
    }
}

export default handlePush