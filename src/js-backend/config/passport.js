var LocalStrategy = require('passport-local').Strategy;
var Administrator = require('../models/administrator');

import {
    LOGIN_STATUS_ERROR,
    LOGIN_STATUS_SUCCESS
} from '../../js/constants'

export default (passportObj) => {

    // Session setup
    passportObj.serializeUser(function(administrator, done) {
        console.log('SERIALISING USER');
        done(null, administrator.id);
    });

    passportObj.deserializeUser(function(id, done) {
        console.log('DESERIALISING USER');
        Administrator.findById(id, function(err, administrator) {
            done(err, administrator);
        });
    });

    // Local signup
    passportObj.use('local-login', new LocalStrategy(
    {
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, username, password, done) {
        // Look for user in admin database which matches provided username
        Administrator.findOne({'local.username': username}, function(err, administrator) {
            if (err)
                // Database error occurred
                return done(err);

            if (!administrator)
                // No administrator found
                return done(null, false, {'status':LOGIN_STATUS_ERROR,'message':'User does not exist'});

            if (!administrator.validPassword(password))
                // Administrator found, but password invalid
                return done(null, false, {'status':LOGIN_STATUS_ERROR,'message':'Incorrect password'});
            
            // Administrator found and password valid
            return done(null, administrator, {'status':LOGIN_STATUS_SUCCESS,'message':'Login successful'});
        })
    }
    ))

};