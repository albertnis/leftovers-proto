const mongoose = require('mongoose');
const bcrypt   = require('bcrypt-nodejs');

// ADMINISTRATOR SCHEMA
// Used for Leftovers administrator access to admin panel via passport

const administratorSchema = mongoose.Schema({
    local: {
        username: String,
        password: String
    },
    name: String
});

// Authentication methods

administratorSchema.methods.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

administratorSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('Administrator', administratorSchema, "administrator");
