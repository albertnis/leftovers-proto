const mongoose = require('mongoose');

const vendorSchema = mongoose.Schema({
    name: { type: String, required: true },
    address: { type: String, required: true },
    instructions: { type: String, required: true },
    addedOn: { type: Date, required: true },
    location: {
        type: { type: String, enum: ['Point'] },
        coordinates: { type: [Number], index: '2dsphere' }
    },  
    reps: { type: [String], required: false }
});

module.exports = mongoose.model('Vendor', vendorSchema, 'vendor');
