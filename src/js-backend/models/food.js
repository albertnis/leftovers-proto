const mongoose = require('mongoose');

const foodSchema = mongoose.Schema({
    name: { type: String, required: true },
    imageUrl: { type: String, required: true },
    serves: { type: Number, required: true },
    addedOn: { type: Date, required: true },
    location: {
        type: { type: String, enum: ['Point'] },
        coordinates: { type: [Number], index: '2dsphere' }
    },  
    vendor: { type: String, required: true },
    claimants: { type: [String], required: false }
});

module.exports = mongoose.model('Food', foodSchema ,'food');
