const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    fb: { type: String, required: true, unique: true },
    regcode: { type: String, required: true, unique: true },
    registered: { type: Boolean, required: true },
    addedOn: { type: Date, required: true },
    registeredOn: { type: Date, required: false },
    location: {
        type: { type: String, enum: ['Point'] },
        coordinates: { type: [Number], index: '2dsphere' }
    }
});

module.exports = mongoose.model('User', userSchema, 'user');
