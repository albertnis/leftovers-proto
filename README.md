# Leftovers Prototype

Early proof-of-concept prototype for the Leftovers administrator/database backend. 

The built Javascript is included for simple running. If you don't want to do any module installation or source code development, skip steps 4 and 5 below.


## Installation

Run the following in an appropriate folder for saving the files. You'll need to [install git][git].

1. Clone or download this repo. 

    ```bash
    git clone https://albertnis@bitbucket.org/albertnis/leftovers-proto.git
    ```

1. [Install][mongo] and run MongoDB server in a background command prompt

    ```bash
    mongod
    ```

1. Restore test database from included dump

    ```bash
    mongorestore dump
    ```

1. [Install][nodenpm] node.js/npm and then install dependencies

    ```bash
    npm install
    ```

1. Build backend js, frontend js and css with the 'run' script

    ```bash
    npm run-script run
    ```

1. Serve backend

    ```bash
    node back-output.js
    ```

1. Access port 3000 from web browser. The ```register```, ```login```, and ```admin``` pages are present. e.g. ```localhost:3000/register```

[git]: https://git-scm.com/download/win
[nodenpm]: https://nodejs.org/en/download/
[mongo]: https://docs.mongodb.com/manual/installation/