const webpack = require('webpack');
var etwp = require('extract-text-webpack-plugin')
var ep = new etwp({
  filename: 'main.bundle.css',
  allChunks: true
})

const frontend = {
  entry: ['./src/js/front.js', './src/scss/leftovers.scss'],
  output: {
    path: __dirname + '/public',
    filename: 'front-output.js'
  },
  devtool: 'cheap-module-source-map',
  devServer: {
      publicPath: __dirname + '/public',
      inline: true,
      port: 8080
  },
  module: {
    rules: [
      { 
          test: /\.jsx?$/, 
          loader: 'babel-loader', 
          exclude: /node_modules/,
          query: {
            presets: ['es2017', 'react', 'stage-2']
          }
      },
      {
        test: /\.css$/,
        use: ep.extract({
          use: ['css-loader']
        })
      },
      {
        test: /\.scss$/,
        use: ep.extract({
          use: ['css-loader', 'sass-loader']
        }) 
      }
    ]
  },
  plugins: [
    ep
  ]
};

/* const style = {
  entry: './src/scss/main.scss',
  devtool: 'eval-source-map',
  output: {
    path: __dirname + '/public',
    filename: './main.bundle.css'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: etwp.extract({
          use: 'css-loader'
        })
      },
      {
        test: /\.(sass|scss)$/,
        use: etwp.extract([
          {
            use: 'css-loader'
          },
          {
            use: 'sass-loader'
          }
        ])
      }
    ]
  },
  plugins: [
    new etwp({
      filename: './public/main.bundle.css',
      allChunks: true
    })
  ]
}; */

const backend = {
  entry: './src/js-backend/back.js',
  output: {
    path: __dirname,
    filename: 'back-output.js',
    libraryTarget: 'commonjs'
  },
  
  target: 'node',

  externals: [
    /^(?!\.|\/).+/i
  ],
  devtool: 'eval-source-map',
  module: {
    rules: [
      { 
        test: /\.jsx?$/, 
        loader: 'babel-loader', 
        exclude: /node_modules/,
        query: {
          presets: ['es2017', 'react', 'stage-2']
        }
      },
      {
        test: /\.node$/,
        exclude: /node_modules/,
        loader: 'node-loader'
      }
    ]
  }
};

module.exports = [frontend, backend];